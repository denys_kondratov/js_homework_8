// Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.

const changeValue = () => {
   const leftBtn = document.getElementById("leftBtn");
   const rightBtn = document.getElementById("rightBtn");

   let leftVal = leftBtn.value;
   let rightVal = rightBtn.value;

   leftBtn.value = rightVal;
   rightBtn.value = leftVal;
}

// Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір фону.

window.onload = () => {
   const [...div] = document.getElementsByTagName("div");

   div.forEach(element => {
      element.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 100%, 50%)`;
   })
}

// Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна
// згенерувати тег div з текстом, який був у багаторядковому полі. багаторядкове поле слід очистити після
// переміщення інформації

const valueWrap = () => {
   const text1 = document.getElementById("textWrap"),
   div2 = document.createElement("div"),
   p2 = document.getElementById("wrapText");

   div2.textContent = text1.value;

   p2.append(div2);
   
   text1.value = '';
}

// Створіть картинку та кнопку з назвою "Змінити картинку"
// зробіть так щоб при завантаженні сторінки була картинка
// https://itproger.com/img/courses/1476977240.jpg
// При натисканні на кнопку вперше картинка замінилася на
// https://itproger.com/img/courses/1476977488.jpg
// при другому натисканні щоб картинка замінилася на
// https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png

const changeImg = () => {
   const img = document.getElementById("img1"),
   imgArr = [
      'https://itproger.com/img/courses/1476977488.jpg',
      'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png',
      'https://itproger.com/img/courses/1476977240.jpg'
   ];

   let i = img.alt;   

   if (i!=3) {
      img.src = imgArr[i];       
      i++;
      img.alt = i;
   } else {
      i=0;
      img.src = imgArr[i];
      i++;
      img.alt = i;
   }   
}

// Створіть на сторінці 10 параграфів і зробіть так, щоб при натисканні на параграф він зникав

const section = document.querySelector('#removEl');
 
section.addEventListener('click', e => {
   section.removeChild(e.target);
})